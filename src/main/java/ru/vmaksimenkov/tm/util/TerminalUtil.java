package ru.vmaksimenkov.tm.util;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = nextLine();
        return Integer.parseInt(value);
    }

    static String dashedLine()
    {
        StringBuilder sb = new StringBuilder(20);
        for(int n = 0; n < 20; ++n)
            sb.append('-');
        sb.append(System.lineSeparator());
        return sb.toString();
    }

}
