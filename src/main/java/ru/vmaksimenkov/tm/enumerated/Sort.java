package ru.vmaksimenkov.tm.enumerated;

import ru.vmaksimenkov.tm.comparator.ComparatorByCreated;
import ru.vmaksimenkov.tm.comparator.ComparatorByDateStart;
import ru.vmaksimenkov.tm.comparator.ComparatorByName;
import ru.vmaksimenkov.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    DATE_START("Sort by date start", ComparatorByDateStart.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

}
