package ru.vmaksimenkov.tm.service;

import ru.vmaksimenkov.tm.api.repository.IProjectRepository;
import ru.vmaksimenkov.tm.api.service.IProjectService;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.model.Project;

import java.util.*;

import static ru.vmaksimenkov.tm.util.ValidationUtil.*;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project add(final String name, final String description) {
        if (isEmpty(name) || isEmpty(description)) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (!checkIndex(index, projectRepository.size())) return null;
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneByName(final String name) {
        if (isEmpty(name)) return null;
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project findOneById(final String id) {
        if (isEmpty(id)) return null;
        return projectRepository.findOneById(id);
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        if (!checkIndex(index, projectRepository.size())) return null;
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project removeOneByName(final String name) {
        if (isEmpty(name)) return null;
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project removeOneById(final String id) {
        if (isEmpty(id)) return null;
        return  projectRepository.removeOneById(id);
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description){
        if (isEmpty(id) || isEmpty(name)) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByName(final String name, final String nameNew, final String description){
        if (isEmpty(name) || isEmpty(nameNew)) return null;
        final Project project = findOneByName(name);
        if (project == null) return null;
        project.setName(nameNew);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description) {
        if (!checkIndex(index, projectRepository.size())) return null;
        if (isEmpty(name)) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(final String id) {
        if (isEmpty(id)) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(final String name) {
        if (isEmpty(name)) return null;
        final Project project = findOneByName(name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(final Integer index) {
        if (!checkIndex(index, projectRepository.size())) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishProjectById(final String id) {
        if (isEmpty(id)) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByName(final String name) {
        if (isEmpty(name)) return null;
        final Project project = findOneByName(name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByIndex(final Integer index) {
        if (!checkIndex(index, projectRepository.size())) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project setProjectStatusById(final String id, final Status status) {
        if (isEmpty(id)) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project setProjectStatusByName(final String name, final Status status) {
        if (isEmpty(name)) return null;
        final Project project = findOneByName(name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project setProjectStatusByIndex(final Integer index, final Status status) {
        if (!checkIndex(index, projectRepository.size())) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public int size() {
        return projectRepository.size();
    }

    @Override
    public boolean existsById(String id) {
        if (isEmpty(id)) return false;
        return projectRepository.existsById(id);
    }

    @Override
    public boolean existsByName(String name) {
        if (isEmpty(name)) return false;
        return projectRepository.existsByName(name);
    }


    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
