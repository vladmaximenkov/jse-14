package ru.vmaksimenkov.tm.service;

import ru.vmaksimenkov.tm.api.repository.IProjectRepository;
import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.api.service.IProjectTaskService;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.model.Task;

import static ru.vmaksimenkov.tm.util.ValidationUtil.*;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    final private ITaskRepository taskRepository;

    final private IProjectRepository projectRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId) {
        if (isEmpty(projectId)) return null;
        if (!taskRepository.existsByProjectId(projectId)) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task bindTaskByProjectId(final String projectId, final String taskId) {
        if (isEmpty(projectId) || isEmpty(taskId)) return null;
        if (!projectRepository.existsById(projectId)) return null;
        return taskRepository.bindTaskPyProjectId(projectId, taskId);
    }

    @Override
    public Task unbindTaskFromProject(final String taskId) {
        if (isEmpty(taskId)) return null;
        return taskRepository.unbindTaskFromProject(taskId);
    }

    @Override
    public Project removeProjectById(final String projectId) {
        if (isEmpty(projectId)) return null;
        if (!projectRepository.existsById(projectId)) return null;
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeOneById(projectId);
    }

    @Override
    public Project removeProjectByName(final String projectName) {
        if (isEmpty(projectName)) return null;
        String projectId = projectRepository.getIdByName(projectName);
        if (isEmpty(projectId)) return null;
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeOneByName(projectName);
    }

    @Override
    public Project removeProjectByIndex(final Integer projectIndex) {
        if (!checkIndex(projectIndex, projectRepository.size())) return null;
        String projectId = projectRepository.getIdByIndex(projectIndex);
        if (isEmpty(projectId)) return null;
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeOneByIndex(projectIndex);
    }

    @Override
    public void clearTasks() {
        taskRepository.removeAllBinded();
        projectRepository.clear();
    }

}
