package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTaskByProjectId(String projectId);

    Task bindTaskByProjectId(String projectId, String taskId);

    Task unbindTaskFromProject(String taskId);

    Project removeProjectById(String projectId);

    Project removeProjectByName(String projectName);

    Project removeProjectByIndex(Integer projectIndex);

    void clearTasks();

}
