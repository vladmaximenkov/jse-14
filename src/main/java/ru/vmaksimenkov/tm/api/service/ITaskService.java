package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.enumerated.Sort;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    Task add(String name, String description);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task updateTaskById(String id, String name, String description);

    Task updateTaskByName(String name, String nameNew, String description);

    Task updateTaskByIndex(Integer index, String name, String description);

    Task startTaskById(String id);

    Task startTaskByName(String name);

    Task startTaskByIndex(Integer index);

    Task finishTaskById(String id);

    Task finishTaskByName(String name);

    Task finishTaskByIndex(Integer index);

    Task setTaskStatusById(String id, Status status);

    Task setTaskStatusByName(String name, Status status);

    Task setTaskStatusByIndex(Integer index, Status status);

    String getIdByIndex(Integer index);

    int size();

    boolean existsById(String id);

    boolean existsByName(String name);

    void add(Task project);

    void remove(Task project);

    void clear();

}
