package ru.vmaksimenkov.tm.api.repository;

import ru.vmaksimenkov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    void add(Project project);

    void remove(Project project);

    void clear();

    int size();

    String getIdByName(String name);

    String getIdByIndex(Integer index);

    Project findOneById(String id);

    boolean existsById(String id);

    boolean existsByName(String name);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

}
