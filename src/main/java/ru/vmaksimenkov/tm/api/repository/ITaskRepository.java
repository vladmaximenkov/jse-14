package ru.vmaksimenkov.tm.api.repository;

import ru.vmaksimenkov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    Task bindTaskPyProjectId(String projectId, String taskId);

    Task unbindTaskFromProject(String taskId);

    List<Task> findAll(Comparator<Task> comparator);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    String getIdByIndex(int index);

    boolean existsById(String id);

    boolean existsByProjectId(String projectId);

    boolean existsByName(String name);

    int size();

    void add(Task task);

    void remove(Task task);

    void removeAllBinded();

    void clear();

    void removeAllByProjectId(String projectId);
}
